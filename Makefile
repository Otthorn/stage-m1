output_file := Build/main_$(shell date +%F).pdf

all: compile

compile: 
	@echo "Compiling the Report"
	@mkdir -p Build 
	pandoc --defaults pandoc_theme/defaults.yml --metadata link-citations=true Report/main.md -o $(output_file)
	
clean:
	@echo "Cleaning up Build directory"
	@rm Build/*.pdf
