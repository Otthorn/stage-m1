# Master 1 Internship - Differential Privacy applied to Crowd Density Estimation

This repository is home to my report and work during my M1 internship with E.
Aldea at the SATIE laboratory in Saclay.

All the work here is licensed under CC BY-NC-SA 4.0 unless stated otherwise.

## Building

To build the content, please use the Makefile which should automatically
generate the report into the `Build` directory. 

```bash
make
```

The `Build` directory and all `.pdf` have been added to the `.gitignore` to order to
minimise the place take in the repository.

## Requirements

This build has been tested and is fully functional with the following
versions:

* tex-live-full: Tex Live 2019/Debian
* pandoc: 2.9.2.1
* pandoc-citeproc: 0.17

## Credit

Crowd photography on the title page by [Chris Barbalis](https://unsplash.com/@cbarbalis)
