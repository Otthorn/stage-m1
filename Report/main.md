---
title: Differential Privacy applied to Crowd Density Estimation
subtitle: M1 internship report
date: Summer 2020
author: Solal Nathan, under the supervision of Emanuel Aldea
header-includes:
- |
  ```{=latex}
  \usepackage{siunitx}
  \usepackage{float}
  \floatplacement{figure}{H}
  \renewcommand{\d}{\mathrm{d}}
  \renewcommand{\epsilon}{\varepsilon}
  \lstset{backgroundcolor=\color[rgb]{1,0.99,0.96},
	numbers=left,stepnumber=5,numberstyle=\tiny,
	basicstyle=\color[rgb]{0.396,0.482,0.514}\ttfamily,
	keywordstyle=\color[rgb]{0.65,0.631,0.596},
	commentstyle=\color[rgb]{0.576,0.631,0.631},
	stringstyle=\color[rgb]{0.49,0.545,0.824},
	numberstyle=\footnotesize\color[rgb]{0.424,0.443,0.769},
	identifierstyle=\color[rgb]{0.396,0.482,0.514},
	frame=single,breaklines=true,}
  ```
bibliography: Report/bibliography.bib

---
![](Images/crowd-unsplash.jpg)
\vfill
![](pandoc_theme/logos.pdf)
\tableofcontents
\newpage

# Foreword

This internship has taken place during the Covid-19 global pandemic and partly
during the lockdown. Consequently, the vast majority of the work was done
remotely. I hope to have given the best I could under these non-optimal work
conditions and that the quality of the work is unaffected.

# Introduction

Crowd Counting and Crowd Density Estimation has become increasingly important
in the recent years. Crowd analysis has been receiving attention from a
sociological, political and technical point of view [@Zhan_2008].

## Motivations

The interest for crowd analysis yielded many applications:

### Political

The number of people attending a political meeting is a very debated
subject. Often, there is a disagreement between opposing parties on this
number. The bigger the number, the more importance the event has. It is a
metric used to compare the size of different events. Some of the most famous
examples are the Million Man March, called by Louis Farrakhan. The National
Park Service and the march organisers disagreed on the number of participants,
to the point where Louis Farrakhan threatened to sue Washington D.C. National
Park.  A third party review was held by researchers at Boston University.
To this day, the National Park Service continues to estimate the size of the
crowd for organisational purposes, but do not publicly release this
information.

More recently, the inauguration of Donald Trump drew in less people than stated
by the U.S. Government. The press secretary announced that this was the largest
audience for a presidential inauguration, when in reality it was at best half
of the audience present during Barrack Obama's inauguration in 2009.

Finally, there is the French case of the CGT (General Confederation of Labour)
who overestimates the size of the crowd on a regular basis[^CGT]. After each
protest, the CGT and the Police release the number they estimated for the size
of the crowd who attended the event. Software analysis by the company
Occurrence and independent review by different groups of journalists backed up
very closely the police numbers. In this example, the police estimated \num{8000}
attendees, within an 8 % margin of error of third parties numbers,
while the CGT estimated \num{40000} attendees.

[^CGT]: This is a very politically polarising subject. I am only presenting
  scientific evidence here. I do not have an opinion on this matter. 

The subject has a vast political intricacy, making the accuracy and
transparency of crowd counting algorithm of high importance.

### Safety

Every meeting which draws a large crowd can potentially be dangerous.
Religious gatherings, political meetings or sport events can gather very large
crowds. Without any external phenomenons, the crowd can become dangerous at a
high density. Lack of air, 'crowd quake' and tramples can happen. Correctly
evaluating the density and reacting is crucial to ensure the safety of
participants [@Helbing_2012; @Moussaid_2011].

The maximum density considered safe is 5 person per square meter, above this
threshold the risk is considered to be high [@Moussaid_2011].

![Representation of the maximum accepted density: 5 people per square meter](Images/density_5_sqm.png){width=60%}

Moreover, external stimuli can provoke stampedes. Terrorist attacks or any
other risk to the crowd can lead to injuries and deaths due to the crowd
movements [@Crowd_2007].

### Public space design

By using crowd analysis and computer simulations, better public spaces,
sometimes referred as "intelligent environments", can be built.
Sociologist, psychologist and civil engineers are able to better manage the
crowd and the flux of people in museums for example [@Helbing_2000;
@Obstacle_2019].

## Ethics and Privacy

The ethics of algorithms is a subject of paramount of importance in a world
where the lives of billions is greatly influenced by algorithms.

The progress of Artificial Intelligence (IA) in Computer Vision (CV) enables
the automated analysis of large quantities of data, which was not possible
decades ago. As a side effect, it also enables entities that have access to
large quantities of data to do mass surveillance. For example, the Chinese
government created an automated ranking system, known as the Social Credit
System, with the help of their mass surveillance system: Skynet[^1].

[^1]: Skynet Project is China's national surveillance system, composed of
more than \num{20} millions cameras. It is the translation of the Chinese name
"Tianwang" and has nothing to do with the evil AI in Terminator movies.

Moreover, negative behaviours can emerge even with good and moral intent in AI.
The alignment problem is extremely difficult. More research and funds would
need to be devoted to the alignment problem according to field experts.

For example, recommender systems are AI which influence humans decision[^2] by
filtering results and recommending content. It is widely used in social media
and e-commerce websites. Many problems arise from recommender systems, such as:

[^2]: Youtube has 1 billion daily users, with 30 minutes of average daily watch
  time. 2/3 of watched videos are recommended by Youtube's algorithm.
  Social media can greatly influence it's users emotions based on the content
  they are exposed to.

**Filter bubbles:**
Creating isolated online filters which reinforce the users
own ideas. By separating the user from sources of informations which
disagree with their point of view, algorithms can form radical online
communities. It can greatly influence public opinion, for example in the case
of anti-vaccination communities or before presidential elections.

**Human Bias:**
Machine Learning algorithms have to learn from data. Sometimes
this data can be human-generated and reflect a human bias like sexism or
racism. For example, Word2Vec is a NLP algorithm which learn a to map word in a
semantic space. You can then perform simple algebraic operations to combine
them.

```
King -> Male; Female -> ?
> Queen
```

But asking Word2Vec:

```
Father -> Doctor; Mother -> ?
> Nurse
```

Which means that the algorithm learned from a bias in our language present in
the corpus it was trained on. It leads to "hard de-biasing" from researchers to
fix this issue.

Another example is the Google Image results for CEO. It used to return a vast
majority of male persons, but was fixed to included more female persons. This
case is complex since it conveys reality, there is an overwhelming number of
male CEO compared to female CEO in the top 500 Fortune Companies. Yet, it is
not considered morally correct and has to be addressed. This could even help to
solve the issue in this future.

**Trying to enforce an idea can lead to the opposite effect:**
When trying to improve a complex system, it is a difficult task to anticipate
the changes that would lead to the best outcome. It is in fact possible to
worsen the situation. A machine learning algorithm with a large number of
weights is very complex and far from transparent.

<!-- Add Facebook local vs global experiment ? --> 

The ethics of algorithms is an important and difficult problem. Recommender
systems have a strong influence on emotions and decisions making of human
beings.

Finally, the Open Data movement has been growing in an effort from organisations
and governments to release anonymised data to the public. It enables developers
and statisticians to access real world data, sometimes from live sources like
transportation, quality of air in a city or census data on the population
height, to give a few examples.

<!-- Add Facebook emotion study, or the fact that 2/3 of youtube video are
recommended and 1 billion daily users with 30 min average per day 
?
-->

## Goal

The goals of this study are to get a better understanding of the two subjects
at hand: crowd density estimation and differential privacy.  Reading
bibliography and being able to synthesize the state of the art in both domains.

To achieve this goal, different approaches have been taken.
First an iterative approach, prototyping in Python and working from the ground
up to harder problems. Utilising an interactive approach, working in less
dimensions to make the problem easier to tackle. It also gives a better
understanding of it, which can be used for higher dimensions and make the
explanation of the results easier to follow for the reader.

The goal is also to use state of the art metrics, understand them and
reformulate the problem in term of those metrics.

Crowd density estimation problems deal with sets of images of the crowd.  A
density map has to be extracted in order to best represent the local density of
people in the image. The goal is to make this information blurry enough so that
an adversarial algorithm cannot infringe individual privacy - for example by
being able to follow someone. Yet, it is important to retain the capacity to
make decisions to ensure crowd safety, the design of intelligent environment
and to study group behaviour.

By creating a framework which enables data hoarders to make it publicly
available without the fear to put the privacy of its users at risk, the goal is
to enable researchers to have access to a larger quantity of data.

In order to be trustworthy enough, the robustness of the method against
adversarial algorithm has to be proven. Unfortunately, formal methods are very
difficult to use in machine learning and a probabilistic approach to privacy
based on compromises is often used to replaced them.

# Presentation of the Laboratory

The SATIE is a laboratory of the *ENS Paris-Saclay*. It is a UMR (*Unité Mixte de
Recherche*) of the *CNRS* born in the mid 1970s from the Electrical
Engineering Department of the *ENS Paris-Saclay* (*ENS Cachan* at the time).
It is supported by five colleges and 2 national research institutes: *ENS
Paris-Saclay, Cergy Paris Université, le CNAM Paris, ENS Rennes, Université
Paris-Salcay, Université Gustave Eiffel* and *CNRS*.

The name *SATIE* stands for *Système des Applications des Technologies de
l'Information et de l'Énergie* (Systems and Applications of Information and
Energy Technologies).

The research at the SATIE laboratory is dedicated to both Electrical
Engineering and Applied Physics. Its domains of applications range from health
imagery, to sustainable development and energy conversion.

My internship supervisor was Emanuel Aldea, Associate Professor at the
*Université Paris-Saclay* and researcher at the *SATIE* in the *MOSS*
department (*Méthodes et Outils pour les Signaux et Systèmes*). 

The *SATIE* is divided into two research groups: *Electric Energy Systems* and
*Systems for multiscale information measurement and analysis*.

When I had to chance to be physically present in the laboratory, I went to
its new location, Building 660 (Digiteo) in Saclay, near the ENS Paris-Saclay.

# State of the Art

## Differential Privacy

Differential Privacy is a formal framework which aims for robustness and
universality against adversarial attacks trying to infer personal informations
from public datasets. [@lecuyer_certified_2019].

The idea is that making a single substitution inside a database has a small
enough impact to not affect the statical values of the database. The goal is
to make a query of the database not usable to infer information about of a
single individual.

First, lets define the core concepts of Differential Privacy. It is a
mathematical framework which comes from cryptography.

$(\varepsilon, \delta$)-Differential Privacy:

 * $\mathcal{A}$ a probabilist algorithm, the adversary
 * $\mathcal{Im(A)}$ the image of the algorithm
 * $\mathcal{S}$ a subset of $\mathcal{Im(A)}$
 * $d$ and $d'$ two databases
 * $\rho$ a distance between databases

$$
P(\mathcal{A}(d) \in \mathcal{S}) \le \exp(\varepsilon) P(\mathcal{A}(d') \in \mathcal{S}) + \delta
$$

With $\varepsilon \ge 0$ and $\delta \in [0,1]$

The idea behind it is trying to give no statistical significance to
individual information. In a sense, if a query is very precise and involve a
small number of people, more noise needs to be added [@Dwork_2006]. 

A mechanism often used in differential privacy is the addition of noise, more
precisely Laplace noise.

$(\varepsilon, \delta$)-Differential Privacy is a relaxed version of the
original $\varepsilon$-Differential Privacy, which generalise the definition and
enables to weaken it in order to accommodate for harder problems.

There is also the concept of k-anonymity which has the goal to solve the
following problem: "Given person-specific field-structured data, produce a
release of the data with scientific guarantees that the individuals who are the
subjects of the data cannot be re-identified while the data remain practically
useful." [@Samarati_1998; @hassan_differential_2019].

Sometimes the concept of Differential Privacy or k-anonimity makes an
assumption about the adversary that it should not make. A correlation attack
from another publicly available dataset can be performed. There is the case of
*mobility datasets* which are relevant to this study. Using geolocalisation
data and data from a subway network can identify uniquely a vast majority of a
*mobility dataset* [@kondor_towards_2018].

Moreover, the concept of k-anonimity ceases to work well for sparse databases
of very high dimensions like a *mobility dataset* [@kondor_towards_2018].

Differential Privacy is a very formal framework and cannot be applied so easily
to every problem. Methodology to follow in order to have a application in CV
can be found in studies such as PixelDP [@lecuyer_certified_2019].

## Crowd Density Estimation

Crowd Density Estimation and Crowd Counting are not recent in the field of
CV. I will rapidly go over traditional approaches in order to focus on more
recent, CNN-based approaches [@sindagi_survey_2018].

**Human approach**, sometimes referred as Jacobs' method where you
estimate the local density in a section of the crowd and multiply it by the
number of sections in the crowd. This is a very archaic method that does not
rely on CV.

**Dectection-based methods** with a classifier running in the sliding windows
and feature identification such as wavelets analysis. They were only successful
in low density crowds. Other iteration of this approach tried to detect
specific body part, but added complexity and human expert design as the
classical approach to AI at the time.

**Regression-based methods** where the algorithm tries to count by
regression the map between features extracted from patch of images to the real
count. It was better for high density, occlusion and background clutter.

**Density estimation-based approaches** which incorporated spatial
information in their algorithm.

At this point, methods based on modern ML such as CNN are presented. It is a
class of deep learning algorithms widely used in analysing visual images. They
have the good property of being space invariant (translation).

**Simple CNN** are very simple CNN methods, just a deep neural network
regularized to be a CNN.

**Scale-aware models** are models focus on robustness to variation of scale.

**Context-aware models** are models which incorporate local and global
information in order to have lower error and more attention to small details as
well as large ones.

**Multi-task models** are a combinaison of different approaches.

A plethora of methods can be added to gain accuracy based on the specific
problem tackled such as, but not limited to: LSTM decoders, boosting, transfer
learning or mixture of CNN (MoC) [@sindagi_survey_2018; @Pellicano_2018]

I will now present the common metrics used in crowd density estimation in order
to judge of the accuracy of an algorithm.

The most common ones are MAE (Mean Absolute Error) and MSE (Mean Square
Error)[@vandoni_ensemble_2019]:

$$ \text{MAE} = \frac{1}{N} \sum_{n=1}^N | y_n - \hat y_n| $$
$$ \text{MSE} = \frac{1}{N} \sum_{n=1}^N (y_n - \hat y_n)^2 $$

A less common but every similar one is the RMSE (Root Mean Squared
Error)[@tian_padnet_2020]:

$$ \text{RMSE} = \sqrt{ \frac{1}{N} \sum_{i=1}^N (y_n - \hat y_n)^2 } $$

Those metrics do not take into account the different scales at hand, which are
very important to our problem. An example of a metric which does take it into
account is the PMAE (Patch Mean Absolute Error)[@tian_padnet_2020]:

$$ \text{PMAE} = \frac{1}{m \times N} \sum_{i=1}^{m \times N} |y_n - \hat y_n| $$

This result aggregates into a single number the MAE at different scales. I
will sometimes use a decomposed form, scale by scale, of the PMAE called the
Multiscale MAE. It is just a vector composed the MAE at the different scales,
and summing it result in the PMAE.


![Multiscale MEA expected in reality](Images/mea_real.png){width=50%}

![Multiscale MEA under ideal circumstances](Images/mea_ideal.png){width=50%}

![Multiscale MEA with the privacy
constrain after a certain scale](Images/mea_constrained.png){width=50%}

It is already clear that a compromise need to be made between clarity of data
and privacy.

# Work

## Foreword

In order to follow the work realised during this internship, I will sometime
take the liberty to present it in chronological order. I will go over the
issues I faced or the errors I made and the ways I found to overcome or bypass
them.

## Introduction

It is very useful to run existing Crowd Density Estimation algorithm to
better understand their structure and the output they provide. This output
constitutes the base of the work I have to do, and working from assumption on
the data can be traitorous. Having real data out of a Crowd Density Estimation
Neural Network can lead to more or less assumptions, making our
privacy-preserving algorithm slightly different.

The same can also be said about the adversary, if it is assumed that the
adversary is all-powerful, not making any assumption about its competences
makes the problem more difficult that it already is. Considering the
computational power it has is crucial. In practice knowing how much the
adversary can modify its belief about the world seeing our output is desirable.
Assuming it already knows everything about the movement in the crowd, it makes
the work very easy, no work has to be done, since the adversary will not get
any information about the crowd from us. On the other hand, if the adversary
knows nothing about the crowd it does not make our work easier.

Finally, if the adversary knows some information about the crowd, from an
external sources (another database, another opendata source, or by gathering
information by itself in order to perform correlation attacks on chosen
targets) it makes our work much more difficult.
For example, the adversary could track a person for a few moments in the crowd.
It would then be able to identify the person using a correlation attack on the
path of the of different data points and then follow this point in the crowd
using an algorithm like K-shortest paths.

The scenarios where the adversary has some information about a subset of the
crowd and tries to infer some other information are difficult and interesting.

A compromise always has to be done, giving away no information about the crowd,
the goals set up in the begging cannot be respected. In some cases,
compromising some privacy in order to extract some data is very important. It
is not always possible to respect k-anonymity, but it could be argued that in
some cases this information can save lives and is more important that privacy.

Thinking about the Covid-19 pandemic and privacy-preserving algorithm, it
becomes a morally thorny question to preserve the strict privacy of
individuals.

Privacy-preserving contact tracing could be very important tools for expert
epidemiologist to better estimated some important parameters like the basic
reproduction number in subsets of the population.

## 1D Case

To begin with a simple approach, we tried to model our problem in a simple 1D
case. The real problem is a 2D density map and third dimension with the time.
The goal is to prevent an adversarial algorithm to gain unwanted knowledge from
our density map.

### Ground truth

First, lets define the ground truth, represented by the position of the
persons in the scene. Let our input space be a subset of $\mathbb{R}$, for the
sake of simplicity and normalisation I choose $\mathcal{S} = [0,1[$. I was then able to
create functions of $\mathcal{S}$ in itself. Let $g : x \mapsto g(x)$ be the
ground truth and $f : x \mapsto f(x)$ be the density map.

For example, 3 person randomly sample in the space $\mathcal{S}$ can be
represented as so:

![1D ground truth $f$ with N=3](Images/gt_4.png){width=60%}

It is possible to consider an approach based on probability theory in which
each of these functions are probability density functions (PDF). In this case,
the ground truth can be considered as a sum a Diracs. In general, in order to
use this formalism, it would always be necessary it ensure that the integral of
our functions over $\mathcal{S}$ are equal to 1. This notion of considering
functions as PDF can be useful in some cases as seen later with optimal
transport distance.

This also contradict a very useful crowd counting property which is to have
every local integral equal to the number of person inside the space integrated.
For example, taking a 200 pixels by 200 pixels squares in the image, the
integral of our density map over this patch should sum up to the number of
people in it.  This needs to be true for every scale in order to have a perfect
PMAE.

### Density Map generation

After having defined the ground truth, I tried to add some Gaussian noise over
each person to model the uncertainty of the crowd density algorithm. As we will
see later, it is similar to the output of the *tiny-heads* algorithm we will
use to generate potential input data.

It can formalised as blurring using a (normalised) Gaussian kernel:

$$ f(x) = \sum_{i=1}^N g(x - x_i) G(x_i, \sigma) $$

![1D density map $g$](Images/dm_3.png){width=60%}

In this image, a normalisation coefficient is not added to ensure the Gaussian
distribution is equal to one, but this will be fixed in later iterations like
the 2D case. It will then be explained how to calculate this coefficient.

The standard deviation $\sigma$ can be adjusted to represent the sizes of the
heads. The hypothesis of $\sigma$ being constant over space is made, it is not
always the case in crowd counting datasets. It is notably the case when an
image is taken from a low camera angle: heads in the foreground are then bigger
than heads in the background.  Here we simplify by making the hypothesis that
$\sigma$ is constant over the image. It is equivalent to considering that the
image is taken from a bird's-eye view.

$\sigma$ is set \num{0.05} in the following examples. It is important to take a
relatively small $\sigma$ compared to the size of $\mathcal{S}$. If $\sigma$ is
too large, it is very easy to fool the adversary, but nothing can be learned of the
density map itself. Choosing a small $\sigma$ is also a better way to learn
the most important factors in fooling the algorithm. Finally, it is a more
realist approach, especially if we make the bird's-eye view hypothesis.

### Adversary algorithm

We then had to create a simple adversary to test the different post processing
we could apply to our data in order to maximise the anonymity, yet retaining
the global density necessary.

A simple algorithm we came up with is the local maximum finding algorithm
described thereafter:

```python
def detection_voting(density_map, alpha):
    """
    Using a voting method in a certain range alpha, try to infer the ground truth from the density map.
    """
    
    N = len(density_map)
    vote = np.zeros(N) # creates an empty vote
    
    # Count the votes
    for k in range(N - alpha):
        index = np.argmax(density_map[k:k+alpha])
        vote[index + k] += 1
        
    # Thresholding  
    for k in range(N):
        if vote[k] > alpha/2:
            vote[k] = 1
        else:
            vote[k] = 0
        
    return vote
```

The parameter $\alpha$ represents the characteristic size given as an heuristic
to the adversary in order to perform well. It has a  great importance in the
performance of the algorithm.

Here, the threshold is set to $\alpha/2$ and will only be used as a tunable
parameter later on.

![Output of the adversarial algorithm (voting map)](Images/voting_map_1.png){width=60%}

I was then able to try this algorithm on a large number of randomly generated
ground truths and density map to evaluate its performances. I first evaluated
its performances on simple density map to be able to later to compare the
results after adding noise or post processing to our data. This will later be
referred as the control case.

To compute the error, the ground truth and the density map (or the resulting
voting map in the case of the adversary) have to be compared.

```
# compact space
gt = [0.2, 0.4, 0.8]

# extended space
gt = [0,0,0,...,0,1,0,...,0,1,0,...0,1,0,...0]
```

In order compare two vectors you can take the norm of their differences.

$$
\epsilon = ||g - \hat g||_2
$$

*Nota:* taking the mean of the difference of the norm of these vectors in a
very large a number of test is the definition of the MAE.

$$
\text{MAE} = \frac{1}{N} \sum_{n=1}^N | g_n - \hat g_n |
$$

I first tried to calculate this difference based on the compact space. However
it turns out that it is very frequent for the algorithm to detect a final
number of person in the image different that the original number of person. In
this case, the norm or two vectors cannot be calculated simply. A trick I came
up with is to penalise heavily those cases with a more complex error function:

$$
\epsilon = \begin{cases}
|| g - \hat g||_2 \quad & \text{if len($g$) = len($\hat g$)}\\
|len(g) - len(\hat g) | & \text{elsewhere}\\
\end{cases}
$$

Where $len(g)$ is the cardinality of $g$, *i.e.* the number of elements
contained by the vector.

This newly defined error function works, but is not very elegant and hard to
balance. The factor in front of the second case should be balanced in order to
have the perfect error function.

Another solution is to consider the L2 norm in the *extended space*. The size
of the vector is always guaranteed to be equal. The cardinality of this vector
is defined when we subdivide the space in order to be able to perform numerical
operations on it. In our case it has a dimension of \num{100}.

Finally, we can perform test on large number of cases (N=\num{10000}) to
compute the MAE as a function of $\alpha$:

![MEA as a function of $\alpha$ before post-processing](Images/1d_error_1.png){width=60%}

This will constitute the basis on which we will compare the rest of the
algorithms. There is almost a linear correlation between $\alpha$ and the MAE,
the lower $\alpha$, the lower the error. This is logical since the algorithm is
more likely to find the true maximum with a lower value of $\alpha$.

### Dirac noise

In order to fool the adversary, it is necessary to conceive a post-processing
effective against it. Adding Dirac noise is likely to work since it will
replace local values of maximum without adding much weight (density) to the
function.

Usually, in DP, the noise added is Laplace noise, or something Gaussian noise,
which would be less fitting in our problem.

Adding the noise does not change drastically the shape of the density map:

![Density map after post-processing with added Dirac noise](Images/1d_dirac_noise.png){width=60%}

After the post-processing, it is possible to recalculate the MAE as a function
of $\alpha$. In a real case scenario we would fix the value of $\alpha$ to the
best value possible obtain on non-noisy data. This is the more logical value to
take for an adversary without any *a priori* on the noise.

![MEA as a function of $\alpha$ with Dirac noise](Images/1d_dirac_mae.png){width=60%}

However, it would be a very poor choice of heuristic for the adversary as the
tendency of the curve is in the opposite way. In this case, the MAE is
inversely correlated to $\alpha$.

A common concept in cryptography is that the system should be safe even if the
enemy knows the algorithm we use - as in open-source algorithm for example.
This principle is illustrated by Shannon's well known sentence "The enemy knows
the system", later formalised as the Kerckhoff's principle.  This contraint
might severely affect the efficiency of our algorithm since a smart adversary
could cherry pick the Diracs, remove them and interpolate a value to replace
them.

This post-processing is successful at fooling the algorithm as the lowest error
obtained in this case is higher than the highest in the control case. Moreover, if
the adversary chooses $\alpha$ based on non-noisy data, the prediction becomes
the worst possible.

### Multiscale MAE

Finally, it is possible to analyse the mutliscale MAE in order to conclude on
the impact of Dirac noise. In this section, $\alpha$ is fixed to 20.

![Mutliscale MAE before the adversary](Images/ms_mae_before.png){width=50%}

![Multiscale MAE after the adversary](Images/ms_mae_after.png){width=50%}

The MAE on the control case after the adversary is better than the one
obtained from the density map. The adversary act as a denoising algorithm.
However, the MAE after the Dirac noise is almost as good the original one for
the first few scales and very degraded after, which is a good compromise between
accuracy and privacy.

## 2D Case

I then proceeded to implement the 2D case. It is more complex to
implement and to compute. The computational complexity is roughly multiplied
by $X = len(x)$ since the algorithm has to go through a square instead of a
line. If we note $\mathcal{O}(n)$ the overall complexity of a given algorithm
in the 1D case, then it will roughly be $\mathcal{O}(X \times n)$.

To simplify the subdivision, notations and relative size while plotting images, I
decided to make a one to one mapping. The meshgrid $x,y$ still has 100
subdivisions, however they now range from 1 to 100.

### Ground truth

Learning from the mistakes I made in the first case, I decided to model
everything in the *extended space* in order to save the hassle of converting
back and forth between spaces.

The only time *compact space* is used, is during the generation of the ground
truth.

![2D ground truth](Images/2d_gt_compact.png){width=60%}

![2D ground truth \label{fig:2d_gt_ext}](Images/2d_gt_extended.png){width=60%}

### Density Map generation

The generation of the density map can then be obtained from the ground truth.
In order to obtain a density map which has the crowd density property, which
is defined as the integral on each subdomain being equal to the number of
person present in the defined domain, it is necessary to normalise the density
map at the level of each added Gaussian.

If 
$$ f(x,y) = A \cdot \exp \left(-\frac{1}{2} \left(\frac{(x - x_0)^2}{\sigma_x^2} + \frac{(y - y_0)^2}{\sigma_y^2} \right)\right)$$

Then
$$ V = \int_{-\infty}^{+\infty} \int_{-\infty}^{+\infty} f(x,y) \d x \d y = 2
\pi A \sigma_x \sigma_y $$

Yet, 
$$ V = 1 \implies A = \frac{1}{2 \pi \sigma_x \sigma_y} $$

Which gives the multiplicative normalisation factor that needs to be applied
the Gaussian in order to respect the crowd density property.

![2D density map \label{fig:2d_density_map}](Images/2d_density_map.png){width=60%}

On this image $\sigma_x = \sigma_y = 5$ (which is equivalent to the $\sigma = 0.05$ from the
previous case since the size of the domain is a hundred times larger).

### Adversary algorithm

The adversarial algorithm has also been rewritten using the knowledge gained
from the previous case.

```python
def detection_voting(density_map, alpha, threshold=0.5):
    """
    Try to detect the position of individuals using the density map.
    Uses a voting methods by looking at the maximum in a square
    of side length alpha.
    
    The end goal is to infer the ground truth from the density map as
    closely as possible.
    """
    
    N = len(density_map)
    vote = np.zeros([N, N]) # creates an empty vote map
    
    # Count the votes
    for x_k in range(N - alpha):
        for y_k in range(N - alpha):
            
            # define the local search space
            square = density_map[x_k:x_k+alpha, y_k:y_k+alpha]
            
            # local coordinate system
            l_x, l_y = np.unravel_index(np.argmax(square, axis=None), square.shape)
            # unravel is necessary to go from the 1D coordinate of the flatten array to the
            # 2D cooridate of the original 2D array. Works in N dimensions.
            
            # global coorinate system
            g_x, g_y = l_x + x_k, l_y + y_k  
            
            vote[g_x, g_y] += 1
            
    # Thresholding
    # vote = np.clip(vote, threshold*alpha**2, np.inf) # because the theoretical maximum is always alpha**2
    for x_k in range(N):
        for y_k in range(N):
            if vote[x_k, y_k] > threshold*alpha**2:
                vote[x_k, y_k] = 1
            else:
                vote[x_k, y_k] = 0
    
    return vote
```

The output of the algorithm on the control case, which means before any
post-processing or noise is added to the density map, can be observe thereafter:

![2D voting map in the control case](Images/2d_voting_map.png){width=60%}

Comparing this figure to Fig \ref{fig:2d_gt_ext} it is clear that the adversary
fails to distinguish very close points due to the spread of the Gaussian
distribution (controlled by $\sigma$). The density map could be considered
noisy by itself due to the sole addition of this Gaussian noise for the
modelling purposes.

The total count goes down from 10 to 7, even if the predictions are very close
to ground truth positions.

This difference can be observe by taking the vector $g - \hat g$, later used in
computing the MAE.

![$g - \hat g$ for the control case \label{fig:diff}](Images/2d_control_diff.png){width=60%}


<!-- TODO:
### Dirac noise

### Random swaps

fill
-->

## Cluster and Slurm

During my work, I had to make use of several algorithms with a high
computational complexity. Most of these algorithms, like training a neural
network, are highly time demanding and optimized for parallel computing on
GPU (or TPU in some cases).

It was simpler to run these algorithm on the cluster for multiple reasons. The
computation of the training of a neural network tend to last for hours. Running
them on a personal computer could take upwards to days, knowing that I do not
own a GPU. It also slows down the machine, making it less available for other
task in the mean time.

Knowning that the algorithm has to run multiple times to tweak certain
hyper-parameters and heuristics, it is important to be able to do it as quickly
as possible and to still be able to perform some other task in the mean time.

Fortunately, the I was able to have an account on the Lab-IA Cluster owned by
the CNRS.

This is a cluster composed of many nodes (servers). To manage the workload,
there is an open-source job scheduler called *Slurm Workload Manager* or
*Slurm* (formerly known as *Simple Linux Utility for Resource Management*).

For more information on how to use *Slurm* see Appendix I.


## Tiny Heads

*Tiny-heads* is a set a of NN which attempts to find tiny heads in crowd
images. It uses a variety of techniques to achieve its goal. It is also more
lightweight than some other competing algorithms in the field.
We decided to use this algorithm in order to better understand the data coming
out of these kind of Crowd Density Estimation networks and their structures.

The network on which we trained our data is the LFE (*Local Feature
Extraction*) [@LFE_2017]. The model is based on the aggregation of local features with
decreasing dilatation vector. It is very efficient at detecting very small
features in images and then scaling them up to the full size of the image
without degradation. It is very important to detect heads, which can be very
small, hence the name *tiny head* of the project which uses it.

Having real world data enables to cut down the modeling phase, which could be
error-prone.  Working with too much or too little assumption about the data can
lead to suboptimal solutions. This is the reason why working directly on real
world data and the ground truth associated to it, is more suitable.

After training the algorithm `main.py` (found in *Appendix II*) for 90 minutes
on the cluster we were able to export the weights given by *PyTorch* in order
to run the test part locally. 

![Test image](Images/test_1.png)

![Density Map estimated by tiny-heads (LFE) after training](Images/output_1.png)

# Discussion

In this section I will discuss certains issues I did not have the time to
tackle and propose solutions that could tested. I will also attempt to idenfity
avenues of reflection for future studies on the subject.

## Optimal transport distance

The issue arising from the metric used by simply comparing the norm is obvious
in Figure \ref{fig:diff}. Very small displacements will be accounted for a +1
in one image and -1 in the other, for a total of 2 added to the MAE. It act as
a Hamming distance, while a more desirable metric would take the distance (in
the space consider) between the two points.

The optimal transport distance takes this aspect into account as well as the
cost of displacing it. It can be viewed as the minimum amount of energy needed
to displace a pile of dirt from one place to another. Formally, it represents
the cost needed to go from one probability distribution to another. It is the
result of an optimisation process and as so it is much more complex to compute,
adding to the already long calculations faced in 2D.

The case where two or more persons are merged into a single one by the
adversarial algorithm is still tricky and might necessitate more reflexion.

It also required to convert the density map to a probability distribution,
which means loosing the desirable crowd density property defined before. A
simple solution would be to normalise the density map to a probability
distribution and store the value to re-normalise it to the crowd density map
distribution later if needed.

```python

crowd_density_factor = np.sum(density_map)
distribution = density_map / crowd_density_factor

[...]

density_map = distribution * crowd_density_factor

```

There is a library written in Python which solves Optimal Transport problems
[@flamary_pot_2017].

## Temporal trace

As seen before, the *mobility dataset* can be attacked through correlation
attack. Some algorithms like KSP (K-shortest Path) are able to track the
position of multiple moving objects in a series of images [@KSP].

The KSP algorithm is similar to Dijkstra or Bellman-Ford, looking for the
k-shortest path instead of a single one. It is based on Yen's algorithm which
compute a single-source K-shortest loopless paths for a graph with non-negative
edge cost.

![Diagram of the internal data flow in KSP](Images/ksp.pdf){width=100%}

The newly added dimension can be an advantage in order to fool the adversary.
Someone who would like anonymity can go through an area of high density in
order to multiply exponentially the number of possible paths that the algorithm
has to take into account. The algorithm will always take all path into account,
but having a very high probability in a large number of them make the correct
prediction more difficult.

<!--
TODO:
 * Talk about the 3 sigma solution to fix the integral over the image
 * Talk about generative adversarial structure (GAN-like) to use machine learning
   to solve this problem
 * PMAE / Multiscale MAE
 * Random Swaps
 * Density box method
 -->

# Conclusion

The results obtained on simple toy models are promising. Training a real
crowd density estimation algorithm helped understand the general form of the
data used, but it should be used as an input to our models for more insight on
real world data. More post-processings like the density box method (giving only
the density for a given square of size $S_k$) or the random swaps method
(randomly swapping bits of information within a given $S_k$ to maintain the local
density) should be tested.

Finally, the temporal dimension is very important and it should be considered
in future works.



\newpage
# Appendix I - Slurm {-}

Quick documentation on how to use *slurm*

 * srun: run a job using slurm
 * squeue: see running jobs and information about them
 * scancel: kill a running job

Example of a *sbatch* script to run `main.py`

```bash
#!/bin/bash
#SBATCH --job-name=tiny_heads
#SBATCH --output=tiny_heads_%j.out
#SBATCH --error=tiny_heads_%j.err
#SBATCH --mail-type=END,FAIL  # Mail events (NONE, BEGIN, END, FAIL, ALL)
#SBATCH --mail-user=solal.nathan@example.com
#SBATCH --ntasks=8
#SBATCH --cpus-per-task=1
#SBATCH --ntasks-per-node=8
#SBATCH --partition=gpu
#SBATCH --gpus:tesla:4
#SBATCH --mem=2gb 		# Maximum memory
#SBATCH --time=06:00:00 # Maximum time allocated

echo "Running main script"
pwd; hostname; date 	# Information before running

srun main.py			# run the algorithm / training

date					# Time and date after
```

To then execute this script you can run:
```bash
sbatch task.py
```

More documentation can be found at: 

 * <https://slurm.schedmd.com/documentation.html>
 * <https://help.rc.ufl.edu/doc/Sample_SLURM_Scripts>
 * <https://gitlab.inria.fr/ctallec/slurm-tuto>

\newpage
# Appendix II - Tiny-heads {-}

```python
import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torch.utils.data import sampler
import torchvision.transforms as T
from torchvision import transforms
from torchvision import utils
import scipy.misc
from tensorboardX import SummaryWriter
from networks.lfe import LFE,LFEx
from networks.unet_milesial import UNet
from early_stopping import EarlyStopping
from init_weights import InitWeights
import numpy as np

import re
from os import path
from skimage import io, util, filters
import functools
from scipy import ndimage, sparse

import statistics
from math import *

import itertools

import time

import preprocessing as pp
import datasets
import validation
import transforms as ctr

dtype=torch.float32

# Cuda setup
print(torch.cuda.device_count())
print(torch.cuda.current_device())
print(torch.cuda.get_device_name(torch.cuda.current_device()))
print(torch.cuda.is_available())

device =torch.device('cuda' if torch.cuda.is_available() else 'cpu')
print('using device:', device)

source = 'Data/'
std = 3
cst = 150
# Images to train / test on
train_idxs=['1008']
valid_idxs = ['1516', '216']
test_idxs = ['2025']

train_mean, train_std = datasets.compute_training_mean_std(source, train_idxs)
print(train_mean, train_std)

#With custom dataset
transform = transforms.Compose(
    [ctr.ImgAugTransform(),
     transforms.ToPILImage(),
     transforms.ColorJitter(brightness=0.3, contrast=0.3),
     transforms.ToTensor(),
     transforms.Normalize(mean=[train_mean], std=[train_std])])

transform_valid = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize(mean=[train_mean], std=[train_std])])

transforms_to_output = transforms.Compose(
    [transforms.ToTensor()])
transforms_to_output_valid = transforms.Compose([transforms.ToTensor()])

crop = ((600, 100), (100, 100))

nimages_w = 3
nimages_h = 1

dataset_training = datasets.CrowdDataset(source, train_idxs, std, crop=crop, flip=True, transform=transform, transforms_to_output=transforms_to_output, nimages_w=nimages_w, nimages_h=nimages_h, cst=cst)
dataset_valid = datasets.CrowdDataset(source, valid_idxs, std, crop=crop, transform=transform_valid, transforms_to_output=transforms_to_output_valid, cst=cst)
dataset_test = datasets.CrowdDataset(source, test_idxs, std, crop=crop, transform=transform_valid, transforms_to_output=transforms_to_output_valid, cst=cst)

train_loader = torch.utils.data.DataLoader(dataset_training, 2, shuffle=False)
valid_loader = torch.utils.data.DataLoader(dataset_valid, 1, shuffle=False)
test_loader = torch.utils.data.DataLoader(dataset_test, 1, shuffle=False)

ratio = datasets.heads_background_ratio_uni(train_loader)
print(ratio)

torch.cuda.empty_cache()

net_name='LFE_1008-525'

epochs = 150
lr = 0.007
model = LFE(mydropout=False).to(device)

init_w = InitWeights(init_type='kaiming')
init_w.init_weights(model)

optimizer = torch.optim.Adam(model.parameters(), lr=lr)
loss_function = nn.MSELoss().to(device)
early_stopping = EarlyStopping(patience=20)

s = './logs_solal/' + net_name
writer = SummaryWriter(log_dir=s)

for e in range(epochs):
    ttll = 0
    print('e: ', e, end=' ')
    model.train()
    for t, (x, y, _) in enumerate(train_loader):
        x = x.to(device, dtype=dtype)
        y = y.to(device, dtype=dtype)
        optimizer.zero_grad()
        out_maps = model(x)
        loss = loss_function(out_maps, y)
        loss.backward()
        optimizer.step()
        ttll += loss.item()
    writer.add_scalar('train_loss_per_epoch', ttll / len(train_loader), e + 1)
    ttll = validation.evaluate(model, valid_loader, writer=writer, step=e + 1, device=device, dtype=dtype,
                               loss_function=loss_function)
    writer.add_scalar('valid_loss_per_epoch', ttll, e + 1)
    mAP = validation.evaluate_accuracy(model, valid_loader, writer=writer, step=e + 1, std=std, train_mean=train_mean,
                                       train_std=train_std, device=device, dtype=dtype, cst=cst)

    print("map: %.2f" % mAP, end='\t')

    if early_stopping.step(ttll, model):
        print("Early stopped at epoch ", e)
        break
best_model = early_stopping.get_best_model()
valloss = validation.evaluate(model, valid_loader, writer=None, step=1000, device=device, dtype=dtype,
                              loss_function=loss_function)
mAP = validation.evaluate_accuracy(model, valid_loader, writer=None, step=1000, std=std, train_mean=train_mean,
                                   train_std=train_std, device=device, dtype=dtype, cst=cst)
print("Best validation: loss= ", valloss, " ", " mAP= ", mAP)

path_to_weights = './weights_solal/' + net_name + '-best'

torch.save(best_model.state_dict(), path_to_weights)
```

\newpage
# Appendix III - Source {-}

The source code for this documents, the images, and the algorithms discussed in
this paper can all be found on the following git repository:

[https://gitlab.crans.org/otthorn/stage-m1](https://gitlab.crans.org/otthorn/stage-m1)

The naming scheme for the different folder should make the navigation easy.
Read the `README.md` if you want to build the latest version of the report
locally.

\newpage
# References
