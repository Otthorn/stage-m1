#!/bin/sh
#SBATCH --job-name=tiny_heads         # name of the job
#SBATCH --partition=all        # request for allocation on the CPU partition
#SBATCH --ntasks=1                  # number of tasks (a single process here)
#SBATCH --cpus-per-task=20          # number of OpenMP threads
# /!\ Caution, "multithread" in Slurm vocabulary refers to hyperthreading.
#SBATCH --time=06:00:00             # maximum execution time requested (HH:MM:SS)
#SBATCH --output=oout%j_df.out          # name of output file
#SBATCH --error=err%j_df.out           # name of error file (here, in common with outp
#SBATCH --gres=gpu:1
###SBATCH --nodes=55
###SBATCH --mem = 32
###SBATCH --requeue
###SBATCH --qos=preempt$
###SBATCH --exclude=n1,n2,n3,n4,n5



srun date
srun hostname
srun echo --------------------------------------
srun pwd
srun echo --------------------------------------
srun pwd
srun echo --------------------------------------
source ~/.bashrc
source activate tiny-heads
#cd ./
#CL_SOCKET_IFNAME=eno1 python3 main.py

python3 -u main.py # -u to not buffer prints and stdout them directly

srun date
