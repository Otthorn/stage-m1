# tiny-heads

Joint effort to find tiny heads in crowd images, from Mekka to Regent's Park
and beyond.

# Fork

This is a fork of the original repository. I do not own the data, some of which
is sensitive and could not be included here.
