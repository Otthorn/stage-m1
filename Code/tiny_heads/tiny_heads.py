import torch
import torch.nn as nn
import torch.optim as optim
import torch.nn.functional as F
from torch.autograd import Variable
from torch.utils.data import DataLoader
from torch.utils.data import sampler
import torchvision.transforms as T
from torchvision import transforms
from torchvision import utils
import scipy.misc
from tensorboardX import SummaryWriter
from networks.lfe import LFE,LFEx
from networks.unet_milesial import UNet
from early_stopping import EarlyStopping
from init_weights import InitWeights
import numpy as np

import re
from os import path
from skimage import io, util, filters
import functools
from scipy import ndimage, sparse

import statistics
from math import *

import itertools

import time

import matplotlib.pyplot as plt

import preprocessing as pp
import datasets
import validation
import transforms as ctr

dtype=torch.float32
#device_id=1

print(torch.cuda.device_count())
print(torch.cuda.current_device())
print(torch.cuda.get_device_name(torch.cuda.current_device()))
print(torch.cuda.is_available())


device =torch.device('cuda' if torch.cuda.is_available() else 'cpu')
#torch.cuda.set_device(device_id)

print('using device:', device)

source = 'Data/'
std = 3
cst = 150
#train_idxs = ['10', '20', '11', '12', '26', '531', '21', '525', '1008']
train_idxs=['1008']
valid_idxs = ['1516', '216']
#test_idxs = ['210', '2025']
test_idxs = ['2025']

train_mean, train_std = datasets.compute_training_mean_std(source, train_idxs)
print(train_mean, train_std)

#With custom dataset
#transform = transforms.Compose([transforms.ToTensor()])
transform = transforms.Compose(
    [ctr.ImgAugTransform(),
     transforms.ToPILImage(),
     transforms.ColorJitter(brightness=0.3, contrast=0.3),
     #transforms.Pad((3,0)),
     transforms.ToTensor(),
     transforms.Normalize(mean=[train_mean], std=[train_std])])

transform_valid = transforms.Compose(
    [transforms.ToTensor(),
     transforms.Normalize(mean=[train_mean], std=[train_std])])

#transforms_to_output = [0, 1, 2]
#transforms_to_output_valid = [0]

transforms_to_output = transforms.Compose(
    [transforms.ToTensor()])
transforms_to_output_valid = transforms.Compose([transforms.ToTensor()])

crop = ((600, 100), (100, 100))

nimages_w = 3
nimages_h = 1

dataset_training = datasets.CrowdDataset(source, train_idxs, std, crop=crop,
                                         flip=True, transform=transform,
                                         transforms_to_output=transforms_to_output,
                                         nimages_w=nimages_w, nimages_h=nimages_h, cst=cst)
dataset_valid = datasets.CrowdDataset(source, valid_idxs, std, crop=crop,
                                      transform=transform_valid,
                                      transforms_to_output=transforms_to_output_valid, cst=cst)
dataset_test = datasets.CrowdDataset(source, test_idxs, std, crop=crop,
                                     transform=transform_valid,
                                     transforms_to_output=transforms_to_output_valid, cst=cst)

train_loader = torch.utils.data.DataLoader(dataset_training, 2,
                                           #nimages_w * nimages_h * batch_size, 
                                           shuffle=False)
valid_loader = torch.utils.data.DataLoader(dataset_valid, 1,
                                           #batch_size, 
                                           shuffle=False)
test_loader = torch.utils.data.DataLoader(dataset_test, 1,
                                          #batch_size, 
                                          shuffle=False)

ratio = datasets.heads_background_ratio_uni(train_loader)
print(ratio)

torch.cuda.empty_cache()

#net_name = 'net4_1008'
#net_name = 'LFE-RP'
#net_name='UNet-U'
net_name='LFE_1008-525' ##### TO CHANGE

epochs = 150

#lr = 0.01
lr = 0.007
#stepsize = 75
#gamma = 0.1

#model = Net4().to(device)
#model = LFE(mydropout=False).to(device)
#model_parameters = filter(lambda p: p.requires_grad, model.parameters())
#params = sum([np.prod(p.size()) for p in model_parameters])
#print(params)

#model = UNet(n_channels=1, n_classes=1, bilinear=True).to(device)
model = LFE(mydropout=False).to(device)

init_w = InitWeights(init_type='kaiming')
init_w.init_weights(model)

optimizer = torch.optim.Adam(model.parameters(), lr=lr)
#scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=stepsize, gamma=gamma)

loss_function = nn.MSELoss().to(device)
#pw=torch.empty(1,1,dtype=torch.float32)
#pw[0][0]=1.0/ratio
#loss_function=nn.BCEWithLogitsLoss(pos_weight=pw).to(device)

early_stopping = EarlyStopping(patience=20)

#s = './logs_rp_final/' + net_name
s = './logs_solal/' + net_name #### TO CHANGE 
writer = SummaryWriter(log_dir=s)

for e in range(epochs):
    #scheduler.step()
    ttll = 0
    print('e: ', e, end=' ')
    model.train()
    for t, (x, y, _) in enumerate(train_loader):
        x = x.to(device, dtype=dtype)
        y = y.to(device, dtype=dtype)
        optimizer.zero_grad()
        out_maps = model(x)
        loss = loss_function(out_maps, y)
        loss.backward()
        optimizer.step()
        ttll += loss.item()
    writer.add_scalar('train_loss_per_epoch', ttll / len(train_loader), e + 1)
    ttll = validation.evaluate(model, valid_loader, writer=writer, step=e + 1, device=device, dtype=dtype,
                               loss_function=loss_function)
    writer.add_scalar('valid_loss_per_epoch', ttll, e + 1)
    mAP = validation.evaluate_accuracy(model, valid_loader, writer=writer, step=e + 1, std=std, train_mean=train_mean,
                                       train_std=train_std, device=device, dtype=dtype, cst=cst)

    # if mAP > best_mAP:
    #     best_mAP = mAP
    #     path_to_weights = './weights_LFE/' + net_name + '_' + str(e) + '-best'
    #     torch.save(model.state_dict(), path_to_weights)

    print("map: %.2f" % mAP, end='\t')

    if early_stopping.step(ttll, model):
        print("Early stopped at epoch ", e)
        break
best_model = early_stopping.get_best_model()
valloss = validation.evaluate(model, valid_loader, writer=None, step=1000, device=device, dtype=dtype,
                              loss_function=loss_function)
mAP = validation.evaluate_accuracy(model, valid_loader, writer=None, step=1000, std=std, train_mean=train_mean,
                                   train_std=train_std, device=device, dtype=dtype, cst=cst)
print("Best validation: loss= ", valloss, " ", " mAP= ", mAP)

#path_to_weights = './weights_rp_final/' + net_name + '-best'
path_to_weights = './weights_solal/' + net_name + '-best'   ##### TO CHANGE

torch.save(best_model.state_dict(), path_to_weights)


