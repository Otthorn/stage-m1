import torch
import torch.nn as nn
import torch.nn.functional as F



##### Local Feature Extraction https://arxiv.org/pdf/1709.00179.pdf #####

#myLFE 3
class LFE(nn.Module):
    def __init__(self, mydropout=False, drop_prob=0.5):
        super(LFE, self).__init__()

        self.mydropout = mydropout
        self.drop_prob = drop_prob

        self.layer1 = nn.Sequential(
            nn.Conv2d(1, 16, 3, padding=1, dilation=1),
            nn.BatchNorm2d(16),
            nn.ReLU()
        )
        self.layer2 = nn.Sequential(
            nn.Conv2d(16, 32, 3, padding=1, dilation=1),
            nn.BatchNorm2d(32),
            nn.ReLU()
        )
        self.layer3 = nn.Sequential(
            nn.Conv2d(32, 48, 3, padding=2, dilation=2),
            nn.BatchNorm2d(48),
            nn.ReLU()
        )
        self.layer4 = nn.Sequential(
            nn.Conv2d(48, 64, 3, padding=2, dilation=2),
            nn.BatchNorm2d(64),
            nn.ReLU()
        )
        self.layer5 = nn.Sequential(
            nn.Conv2d(64, 64, 3, padding=3, dilation=3),
            nn.BatchNorm2d(64),
            nn.ReLU()
        )
        self.layer6 = nn.Sequential(
            nn.Conv2d(64, 64, 3, padding=2, dilation=2),
            nn.BatchNorm2d(64),
            nn.ReLU()
        )
        self.layer7 = nn.Sequential(
            nn.Conv2d(64, 64, 3, padding=2, dilation=2),
            nn.BatchNorm2d(64),
            nn.ReLU()
        )
        self.layer8 = nn.Sequential(
            nn.Conv2d(64, 64, 3, padding=1, dilation=1),
            nn.BatchNorm2d(64),
            nn.ReLU()
        )
        self.layer9 = nn.Sequential(
            nn.Conv2d(64, 32, 3, padding=1, dilation=1),
            nn.BatchNorm2d(32),
            nn.ReLU()
        )
        self.layer10 = nn.Sequential(
            nn.Conv2d(32, 1, 1),
            nn.ReLU()
        )

    def forward(self, x):
        x = self.layer1(x) # 1
        x = self.layer2(x) # 1
        #x = F.dropout(x, p=self.drop_prob, training=self.mydropout)
        x = self.layer3(x) # 2
        x = self.layer4(x) # 2
        ##print(self.dropout)
        x = F.dropout(x, p=self.drop_prob, training=self.mydropout) #x = F.dropout(x, p=0.8, training=self.training)
        x = self.layer5(x) # 3
        x = F.dropout(x, p=self.drop_prob, training=self.mydropout)
        x = self.layer6(x) # 2
        x = self.layer7(x) # 2
        #x = F.dropout(x, p=self.drop_prob, training=self.mydropout)
        x = self.layer8(x) # 1
        x = self.layer9(x) # 1
        #x = F.dropout(x, p=self.drop_prob, training=self.mydropout)
        x = self.layer10(x)
        return x


#myLFE 2
# don't converge
# class LFE(nn.Module):
#     def __init__(self):
#         super(LFE, self).__init__()
#
#         # front end module:
#
#         self.dilation1 = nn.Sequential(
#             nn.Conv2d(1, 16, 3, padding=1, dilation=1),
#             nn.ReLU(),
#             nn.Conv2d(16, 16, 3, padding=1, dilation=1),
#             nn.ReLU()
#         )
#
#         self.dilation2 = nn.Sequential(
#             nn.Conv2d(16, 32, 3, padding=2, dilation=2),
#             nn.ReLU(),
#             nn.Conv2d(32, 32, 3, padding=2, dilation=2),
#             nn.ReLU()
#         )
#
#         self.dilation3 = nn.Sequential(
#             nn.Conv2d(32, 64, 3, padding=3, dilation=3),
#             nn.ReLU(),
#             nn.Conv2d(64, 64, 3, padding=3, dilation=3),
#             nn.ReLU()
#         )
#
#         ### lfe:
#
#         self.dilation3_lfe = nn.Sequential(
#             nn.Conv2d(64, 64, 3, padding=3, dilation=3),
#             nn.ReLU(),
#             nn.Conv2d(64, 64, 3, padding=3, dilation=3),
#             nn.ReLU()
#         )
#
#         self.dilation2_lfe = nn.Sequential(
#             nn.Conv2d(64, 64, 3, padding=2, dilation=2),
#             nn.ReLU(),
#             nn.Conv2d(64, 64, 3, padding=2, dilation=2),
#             nn.ReLU()
#         )
#
#         self.dilation1_lfe = nn.Sequential(
#             nn.Conv2d(64, 64, 3, padding=1, dilation=1),
#             nn.ReLU(),
#             nn.Conv2d(64, 64, 3, padding=1, dilation=1),
#             nn.ReLU()
#         )
#
#         ### head module:
#
#         self.headmodule = nn.Sequential(
#             nn.Conv2d(64, 128, 3, padding=3, dilation=3),
#             nn.Conv2d(128, 1, 1),
#             nn.ReLU()
#         )
#
#     def forward(self, x):
#         x = self.dilation1(x)
#         x = self.dilation2(x)
#         x = self.dilation3(x)
#         x = self.dilation3_lfe(x)
#         x = self.dilation2_lfe(x)
#         x = self.dilation1_lfe(x)
#         x = self.headmodule(x)
#         return x


# #myLFE
# class LFE(nn.Module):
#     def __init__(self):
#         super(LFE, self).__init__()
#
#         self.layer1 = nn.Sequential(
#             nn.Conv2d(1, 16, 3, padding=1),
#             nn.BatchNorm2d(16),
#             nn.ReLU()
#         )
#         self.layer2 = nn.Sequential(
#             nn.Conv2d(16, 32, 3, padding=1, dilation=1),
#             nn.BatchNorm2d(32),
#             nn.ReLU()
#         )
#         self.layer3 = nn.Sequential(
#             nn.Conv2d(32, 48, 3, padding=2, dilation=2),
#             nn.BatchNorm2d(48),
#             nn.ReLU()
#         )
#         self.layer4 = nn.Sequential(
#             nn.Conv2d(48, 64, 3, padding=3, dilation=3),
#             nn.BatchNorm2d(64),
#             nn.ReLU()
#         )
#         self.layer5 = nn.Sequential(
#             nn.Conv2d(64, 64, 3, padding=3, dilation=3),
#             nn.BatchNorm2d(64),
#             nn.ReLU()
#         )
#         self.layer6 = nn.Sequential(
#             nn.Conv2d(64, 64, 3, padding=3, dilation=3),
#             nn.BatchNorm2d(64),
#             nn.ReLU()
#         )
#         self.layer7 = nn.Sequential(
#             nn.Conv2d(64, 64, 3, padding=2, dilation=2),
#             nn.BatchNorm2d(64),
#             nn.ReLU()
#         )
#         self.layer8 = nn.Sequential(
#             nn.Conv2d(64, 32, 3, padding=1, dilation=1),
#             nn.BatchNorm2d(32),
#             nn.ReLU()
#         )
#         self.layer9 = nn.Sequential(
#             nn.Conv2d(32, 1, 1),
#             nn.BatchNorm2d(1)
#         )
#         self.layer10 = nn.ReLU()
#
#     def forward(self, x):
#         x = self.layer1(x)
#         # print(x.shape)
#         x = self.layer2(x)
#         # print(x.shape)
#         x = self.layer3(x)
#         # print(x.shape)
#         x = self.layer4(x)
#         # print(x.shape)
#         x = self.layer5(x)
#         # print(x.shape)
#         x = self.layer6(x)
#         # print(x.shape)
#         x = self.layer7(x)
#         # print(x.shape)
#         x = self.layer8(x)
#         # print(x.shape)
#         x = self.layer9(x)
#         # print(x.shape)
#         x = self.layer10(x)
#         return x



##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### ##### #####

## Small number of filters
# class LFE(nn.Module):
#     def __init__(self):
#         super(LFE, self).__init__()
#
#         # front end module:
#
#         self.dilation1 = nn.Sequential(
#             nn.Conv2d(1, 16, 3, padding=1, dilation=1),
#             nn.ReLU(),
#             nn.Conv2d(16, 16, 3, padding=1, dilation=1),
#             nn.ReLU()
#         )
#
#         self.dilation2 = nn.Sequential(
#             nn.Conv2d(16, 32, 3, padding=2, dilation=2),
#             nn.ReLU(),
#             nn.Conv2d(32, 32, 3, padding=2, dilation=2),
#             nn.ReLU()
#         )
#
#         self.dilation3 = nn.Sequential(
#             nn.Conv2d(32, 64, 3, padding=3, dilation=3),
#             nn.ReLU(),
#             nn.Conv2d(64, 64, 3, padding=3, dilation=3),
#             nn.ReLU(),
#             nn.Conv2d(64, 64, 3, padding=3, dilation=3),
#             nn.ReLU()
#         )
#
#         ### lfe:
#
#         self.dilation3_lfe = nn.Sequential(
#             nn.Conv2d(64, 64, 3, padding=3, dilation=3),
#             nn.ReLU(),
#             nn.Conv2d(64, 64, 3, padding=3, dilation=3),
#             nn.ReLU(),
#             nn.Conv2d(64, 64, 3, padding=3, dilation=3),
#             nn.ReLU()
#         )
#
#         self.dilation2_lfe = nn.Sequential(
#             nn.Conv2d(64, 64, 3, padding=2, dilation=2),
#             nn.ReLU(),
#             nn.Conv2d(64, 64, 3, padding=2, dilation=2),
#             nn.ReLU()
#         )
#
#         self.dilation1_lfe = nn.Sequential(
#             nn.Conv2d(64, 64, 3, padding=1, dilation=1),
#             nn.ReLU(),
#             nn.Conv2d(64, 64, 3, padding=1, dilation=1),
#             nn.ReLU()
#         )
#
#         ### head module:
#
#         self.headmodule = nn.Sequential(
#             nn.Conv2d(64, 128, 3, padding=3, dilation=3),
#             nn.Conv2d(128, 1, 1),
#             nn.ReLU()
#         )
#
#     def forward(self, x):
#         x = self.dilation1(x)
#         x = self.dilation2(x)
#         x = self.dilation3(x)
#         x = self.dilation3_lfe(x)
#         x = self.dilation2_lfe(x)
#         x = self.dilation1_lfe(x)
#         x = self.headmodule(x)
#         return x




##### First three layers of VGG-16
class LFEx(nn.Module):
    def __init__(self,mydropout=False, drop_prob=0.5):
        super(LFEx, self).__init__()

        # front end module:

        self.dilation1 = nn.Sequential(
            nn.Conv2d(1, 64, 3, padding=1, dilation=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True),
            nn.Conv2d(64, 64, 3, padding=1, dilation=1),
            nn.BatchNorm2d(64),
            nn.ReLU()
        )

        self.dilation2 = nn.Sequential(
            nn.Conv2d(64, 128, 3, padding=2, dilation=2),
            nn.BatchNorm2d(128),
            nn.ReLU(inplace=True),
            nn.Conv2d(128, 128, 3, padding=2, dilation=2),
            nn.BatchNorm2d(128),
            nn.ReLU(inplace=True)
        )

        self.dilation3 = nn.Sequential(
            nn.Conv2d(128, 256, 3, padding=3, dilation=3),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True),
            nn.Conv2d(256, 256, 3, padding=3, dilation=3),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True),
            nn.Conv2d(256, 256, 3, padding=3, dilation=3),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True)
        )

        ### lfe:

        # self.dilation3_lfe = nn.Sequential(
        #     nn.Conv2d(256, 256, 3, padding=3, dilation=3),
        #     nn.BatchNorm2d(256),
        #     nn.ReLU(inplace=True),
        #     nn.Conv2d(256, 256, 3, padding=3, dilation=3),
        #     nn.BatchNorm2d(256),
        #     nn.ReLU(inplace=True),
        #     nn.Conv2d(256, 256, 3, padding=3, dilation=3),
        #     nn.BatchNorm2d(256),
        #     nn.ReLU(inplace=True)
        # )

        self.dilation2_lfe = nn.Sequential(
            nn.Conv2d(256, 256, 3, padding=2, dilation=2),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True),
            nn.Conv2d(256, 256, 3, padding=2, dilation=2),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True)
        )

        self.dilation1_lfe = nn.Sequential(
            nn.Conv2d(256, 256, 3, padding=1, dilation=1),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True),
            nn.Conv2d(256, 256, 3, padding=1, dilation=1),
            nn.BatchNorm2d(256),
            nn.ReLU(inplace=True)
        )

        ### head module:

        self.headmodule = nn.Sequential(
            #nn.Conv2d(256, 512, 5, padding=6, dilation=3),
            #nn.Conv2d(256, 512, 3, padding=3, dilation=3),
            nn.Conv2d(256, 1, 1),
            nn.ReLU(inplace=True)
        )

    def forward(self, x):
        x = self.dilation1(x)
        x = self.dilation2(x)
        x = self.dilation3(x)
        #x = self.dilation3_lfe(x)
        x = self.dilation2_lfe(x)
        x = self.dilation1_lfe(x)
        x = self.headmodule(x)
        return x
