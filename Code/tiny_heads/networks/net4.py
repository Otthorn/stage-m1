import torch
import torch.nn as nn


class Net4(nn.Module):
    def __init__(self):
        super(Net4, self).__init__()

        self.output_crop = 0  # How much output is cropped wrt input because of
        # convolutions. Typically 0 if you use padding,
        # >0 otherwise.

        self.layer1 = nn.Sequential(
            nn.Conv2d(1, 16, 3, padding=6),
            nn.BatchNorm2d(16),
            nn.ReLU(),
            nn.MaxPool2d(2, stride=1, padding=1))
        self.layer2 = nn.Sequential(
            nn.Conv2d(16, 32, 4, dilation=2, padding=2),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, stride=1, padding=1))
        self.layer3 = nn.Sequential(
            nn.Conv2d(32, 48, 4, dilation=2, padding=2),
            nn.BatchNorm2d(48),
            nn.ReLU(),
            nn.MaxPool2d(2, stride=1, padding=1))
        self.layer4 = nn.Sequential(
            nn.Conv2d(48, 64, 4, dilation=2, padding=2),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, stride=1, padding=1))
        self.layer5 = nn.Sequential(
            nn.Conv2d(64, 64, 4, dilation=2, padding=2),
            nn.Dropout2d(p=0),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, stride=1, padding=1))
        self.layer6 = nn.Sequential(
            nn.Conv2d(64, 64, 4, padding=2, dilation=2),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, stride=1, padding=1))
        self.layer7 = nn.Sequential(
            nn.Conv2d(64, 64, 4, padding=1, dilation=2),
            nn.BatchNorm2d(64),
            nn.ReLU(),
            nn.MaxPool2d(2, stride=1, padding=1))
        self.layer8 = nn.Sequential(
            nn.Conv2d(64, 32, 4, padding=1, dilation=2),
            nn.BatchNorm2d(32),
            nn.ReLU(),
            nn.MaxPool2d(2, stride=1, padding=1))
        self.layer9 = nn.Sequential(
            nn.Conv2d(32, 1, 1),
            nn.BatchNorm2d(1))
        self.layer10 = nn.Sequential(
            nn.ReLU())

    def forward(self, x):
        # print("-------Dimensions------")
        # print(x.shape)
        x = self.layer1(x)
        # print(x.shape)
        x = self.layer2(x)
        # print(x.shape)
        x = self.layer3(x)
        # print(x.shape)
        x = self.layer4(x)
        # print(x.shape)
        x = self.layer5(x)
        # print(x.shape)
        x = self.layer6(x)
        # print(x.shape)
        x = self.layer7(x)
        # print(x.shape)
        x = self.layer8(x)
        # print(x.shape)
        x = self.layer9(x)
        # print(x.shape)
        x = self.layer10(x)
        # print(x.shape)
        return x
