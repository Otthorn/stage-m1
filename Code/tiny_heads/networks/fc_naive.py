import torch
import torch.nn as nn

##### Fully Convolutional 10 layers #####
class FCN10(nn.Module):
    def __init__(self):
        super(FCN10, self).__init__()

        self.layer1 = nn.Sequential(
            nn.Conv2d(1, 16, 3, padding=1),
            nn.BatchNorm2d(16),
            nn.ReLU()
        )
        self.layer2 = nn.Sequential(
            nn.Conv2d(16, 32, 5, padding=2, dilation=1),
            nn.BatchNorm2d(32),
            nn.ReLU()
        )
        self.layer3 = nn.Sequential(
            nn.Conv2d(32, 48, 5, padding=2, dilation=1),
            nn.BatchNorm2d(48),
            nn.ReLU()
        )
        self.layer4 = nn.Sequential(
            nn.Conv2d(48, 64, 5, padding=2, dilation=1),
            nn.BatchNorm2d(64),
            nn.ReLU()
        )
        self.layer5 = nn.Sequential(
            nn.Conv2d(64, 64, 5, padding=4, dilation=2),
            nn.BatchNorm2d(64),
            nn.ReLU()
        )
        self.layer6 = nn.Sequential(
            nn.Conv2d(64, 64, 5, padding=4, dilation=2),
            nn.BatchNorm2d(64),
            nn.ReLU()
        )
        self.layer7 = nn.Sequential(
            nn.Conv2d(64, 64, 5, padding=4, dilation=2),
            nn.BatchNorm2d(64),
            nn.ReLU()
        )
        self.layer8 = nn.Sequential(
            nn.Conv2d(64, 32, 5, padding=4, dilation=2),
            nn.BatchNorm2d(32),
            nn.ReLU()
        )
        self.layer9 = nn.Sequential(
            nn.Conv2d(32, 1, 1),
            nn.BatchNorm2d(1)
        )
        self.layer10 = nn.ReLU()

    def forward(self, x):
        # print("-------Dimensions------")
        # print(x.shape)
        x = self.layer1(x)
        # print(x.shape)
        x = self.layer2(x)
        # print(x.shape)
        x = self.layer3(x)
        #print(x.shape)
        x = self.layer4(x)
        #print(x.shape)
        x = self.layer5(x)
        #print(x.shape)
        x = self.layer6(x)
        # print(x.shape)
        x = self.layer7(x)
        # print(x.shape)
        x = self.layer8(x)
        # print(x.shape)
        x = self.layer9(x)
        # print(x.shape)
        x = self.layer10(x)
        # print(x.shape)
        return x
